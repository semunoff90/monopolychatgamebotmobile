﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatGameMonopolyTgBot
{
	class Room
	{
		public Room() { }

		public Room(string name, int priv = 0, int max_member = 4)
		{
			this.name = name;
			this.priv = priv;
			this.max_member = max_member;
		}
		[Key]
		public int id { get; set; }
		public int priv { get; set; }
		public int max_member { get; set; }
		public string name { get; set; }

	}
}

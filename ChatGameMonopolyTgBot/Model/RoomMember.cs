﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatGameMonopolyTgBot
{
	class RoomMember
	{
		public RoomMember(){ }
		public RoomMember(long id_chat, int id_room, string name)
		{
			this.id_chat = id_chat;
			this.id_room = id_room;
			this.name = name;
		}

		[Key]
		public int id_member { get; set; }

		public int id_room { get; set; }
		public long id_chat { get; set; }
		public string name { get; set; }
	}
}

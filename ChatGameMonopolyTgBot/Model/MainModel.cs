﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace ChatGameMonopolyTgBot
{
	class ApplicationContext : DbContext
	{
		public ApplicationContext() : base("DefaultConnection") { }
		public DbSet<Room> Rooms { get; set; } 
		public DbSet<RoomMember> RoomMembers { get; set; }

	}
	class MainModel
	{
	}
}

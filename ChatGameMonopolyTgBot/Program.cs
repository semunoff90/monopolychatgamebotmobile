﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Args;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace ChatGameMonopolyTgBot
{
	class Program
	{
		static Bot bot = new Bot();
		static void Main(string[] args)
		{

			var client = bot.Get();

			client.Result.OnMessage += Bot_OnMessage;
			client.Result.StartReceiving();
			Console.ReadKey();
		}

		private static async void Bot_OnMessage(object sender, MessageEventArgs e)
		{
			var commands = Bot.Commands;
			var message = e.Message;
			var botClient = await bot.Get();

			foreach (var command in commands)
			{
				if (command.Contains(message.Text))
				{
					command.Execute(message, botClient);
					return;
				}
			}
		}
	}
}

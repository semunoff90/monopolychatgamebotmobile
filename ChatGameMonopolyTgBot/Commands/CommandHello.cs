﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace ChatGameMonopolyTgBot
{
	public class CommandHello : Command
	{
		public override string Name => "/start";

		public override async void Execute(Message message, TelegramBotClient client)
		{
			var chatId = message.Chat.Id;
			var messageId = message.MessageId;
			//Действия команды

			await client.SendTextMessageAsync(chatId, $"Hello {message.Chat.FirstName}!");
		}
	}
}

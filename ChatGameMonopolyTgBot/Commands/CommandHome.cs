﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace ChatGameMonopolyTgBot.Commands
{
	public class CommandHome : Command
	{
		public override string Name => "/home";

		public override async void Execute(Message message, TelegramBotClient client)
		{
			var chatId = message.Chat.Id;
			var messageId = message.MessageId;
			//Действия команды

			await client.SendTextMessageAsync(chatId, $"Вы перешли в начало");
			var keyboard = new KeyBoard(2, new[] { "/InRoom", "/CreateRoom" }, "Выберите команду");
			await keyboard.Show(client, message.Chat.Id);
		}
	}
}

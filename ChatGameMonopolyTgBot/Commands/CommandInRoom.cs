﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ChatGameMonopolyTgBot.Commands
{
	class CommandInRoom : Command
	{
		ApplicationContext db = new ApplicationContext();
		Room tmpRoom = new Room();
		RoomMember tmpRoomMember = new RoomMember();
		List<Room> Rooms = new List<Room>();
		public override string Name => "/InRoom";


		public override async void Execute(Message message, TelegramBotClient client)
		{
			var chatId = message.Chat.Id;
			var messageId = message.MessageId;
			//Действия команды
			var result = db.Rooms.SqlQuery("SELECT * FROM Rooms");
			List<string> resultname = new List<string>();

			foreach (var room in result)
			{
				resultname.Add(room.name);
				Rooms.Add(room);
			}

			var keyboard = new KeyBoard(2, resultname.ToArray(), "Выберите комнату из существующих в которую хотите войти:");
			await keyboard.Show(client, chatId);


			client.OnMessage += Bot_OnMessageSelectNameRoom;
			client.StartReceiving();
		}

		private async void Bot_OnMessageSelectNameRoom(object sender, MessageEventArgs e)
		{
			if (e.Message.Type == MessageType.Text)
			{
				var botClient = new Bot().Get();
				botClient.Result.OnMessage -= Bot_OnMessageSelectNameRoom;
				botClient.Result.StopReceiving();
				foreach (var room in Rooms)
				{
					if (e.Message.Text == room.name)
					{
						var result = db.RoomMembers.SqlQuery($"SELECT count(*) FROM RoomMembers WHERE id_room={room.id}");
						if (result.Equals(room.max_member))
						{
							await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id,
								$"В комнате {room.name} максимальное количество участников.");
							new CommandHome().Execute(e.Message, botClient.Result);
							return;
						}

						result = db.RoomMembers.SqlQuery($"SELECT count(*) FROM RoomMembers WHERE id_chat={e.Message.Chat.Id}");
						if (!result.Equals(0))
						{
							await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id,
								$"Вы уже находитесь в комнате");
							new CommandHome().Execute(e.Message, botClient.Result);
							return;
						}

						tmpRoomMember.id_chat = e.Message.Chat.Id;
						tmpRoomMember.name = e.Message.Chat.FirstName;
						tmpRoomMember.id_room = room.id;
						db.RoomMembers.Add(tmpRoomMember);
						db.SaveChanges();
						await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id,
							$"Вы вошли в комнату {room.name}");
						result = db.RoomMembers.SqlQuery($"SELECT Name FROM RoomMembers WHERE id_room={room.id}");
						if(result == null) return;
						string namemembers = "";
						foreach (var roomMember in result)
						{
							namemembers += roomMember.name + '\n';
						}
						await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id, "С вами в комнате:\n"+namemembers);
						var keyboard = new KeyBoard(2, new[] { "/InRoom", "/CreateRoom" }, "Выберите команду");
						await keyboard.Show(botClient.Result, e.Message.Chat.Id);
					}
				}
				await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id, "Такой комнаты не существует");
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using ChatGameMonopolyTgBot.Commands;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace ChatGameMonopolyTgBot
{
	public class CommandCreateRoom : Command
	{
		ApplicationContext db = new ApplicationContext();
		Room tmpRoom = new Room();
		public override string Name => "/CreateRoom";

		public override async void Execute(Message message, TelegramBotClient client)
		{
			var chatId = message.Chat.Id;
			var messageId = message.MessageId;
			//Действия команды

			await client.SendTextMessageAsync(chatId, "*Создание комнаты.*\nУкажите название комнаты:");

			client.OnMessage += Bot_OnMessageNameRoom;
			client.StartReceiving();
		}

		private async void Bot_OnMessageNameRoom(object sender, MessageEventArgs e)
		{
			if (e.Message.Type == MessageType.Text)
			{
				tmpRoom.name = e.Message.Text;
				var botClient = new Bot().Get();
				var keyboard = new KeyBoard(2, new[] { "2", "3", "4" }, "*Создание комнаты.*\nУкажите кол-во участников:");
				await keyboard.Show(botClient.Result, e.Message.Chat.Id);
				botClient.Result.OnMessage -= Bot_OnMessageNameRoom;
				botClient.Result.StopReceiving();
				botClient.Result.OnMessage += Bot_OnMessageCountMember;
				botClient.Result.StartReceiving();
			}
		}

		private async void Bot_OnMessageCountMember(object sender, MessageEventArgs e)
		{
			if (e.Message.Type == MessageType.Text)
			{
				tmpRoom.max_member = Convert.ToInt32(e.Message.Text);
				var botClient = new Bot().Get();

				botClient.Result.OnMessage -= Bot_OnMessageCountMember;
				botClient.Result.StopReceiving();

				tmpRoom.priv = 0;
				db.Rooms.Add(tmpRoom);
				db.SaveChanges();

				await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id, "Комната создана");

				var result = db.Rooms.SqlQuery("SELECT * FROM Rooms");
				string resultname = "";

				foreach (var room in result)
				{
					resultname += room.name + '\n';
				}

				await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id, "Существующие комнаты:");
				await botClient.Result.SendTextMessageAsync(e.Message.Chat.Id, resultname);
				new CommandHome().Execute(e.Message, botClient.Result);
			}
		}
	}
}